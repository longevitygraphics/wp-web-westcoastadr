<div class="py-3 bg-gray-light">
	<div class="container buttons">
		<a href="/dispute-resolution-training-and-workshops/" class="btn btn-primary cta">
			<div>
				DISPUTE RESOLUTION<br>
				<span class="text-secondary">TRAINING & WORKSHOPS</span>
			</div>
			<div class="arrow-right"><i class="fas fa-caret-right"></i></div>
		</a>
		<a href="/family-law-dispute-flow-chart/" class="btn btn-primary cta">
			<div>
				FAMILY LAW DISPUTE<br>
				<span class="text-secondary">FLOW CHART</span>
			</div>
			<div class="arrow-right"><i class="fas fa-caret-right"></i></div>
		</a>
		<a href="/comparison-of-costs/" class="btn btn-primary cta">
			<div>
				<span class="text-secondary">COMPARISON OF COSTS</span>
			</div>
			<div class="arrow-right"><i class="fas fa-caret-right"></i></div>
		</a>
	</div>
</div>
<?php
	$args = array(
        'showposts'	=> -1,
        'post_type'		=> 'member',
        'tax_query' => array(
        array (
            'taxonomy' => 'member-category',
            'field' => 'slug',
            'terms' => 'Staff',
            )
        ),
    );

    $args2 = array(
        'showposts' => -1,
        'post_type'     => 'member',
        'tax_query' => array(
        array (
            'taxonomy' => 'member-category',
            'field' => 'slug',
            'terms' => 'Tribute',
            )
        ),
    );



    $result = new WP_Query( $args );

    // Loop
    if ( $result->have_posts() ) :
    	?>
    	<div class="team-list container">
    	<?php
        while( $result->have_posts() ) : $result->the_post(); 
        	$title = get_the_title();
        	$url = get_permalink();
            $member_image = get_field('member_image');
            $member_certs = get_field('member_subtext');
    	?>
    	
       		<div>
                <a href="<?php echo $url; ?>">
           			<img src="<?php echo $member_image['url']; ?>" alt="<?php echo $member_image['alt']; ?>">
                    <h2 class="h3"><?php echo $title; ?> <?php if($member_certs){ echo '<br>'.$member_certs; } ?></h2>         
                </a>
       		</div>

		<?php
        endwhile;
        ?>
        </div>
        <?php
    endif; // End Loop

    wp_reset_query();

        $result = new WP_Query( $args2 );

    // Loop
    if ( $result->have_posts() ) :
        ?>
        <div class="team-list container">
        
        <?php
        while( $result->have_posts() ) : $result->the_post(); 
            $title = get_the_title();
            $url = get_permalink();
            $member_image = get_field('member_image');
            $member_certs = get_field('member_certs');
        ?>
        
            <div>
                <a href="<?php echo $url; ?>">
                    <img src="<?php echo $member_image['url']; ?>" alt="<?php echo $member_image['alt']; ?>">
                    <h2 class="h3"><?php echo $title; ?> <?php if($member_certs){ echo '<br>'.$member_certs; } ?></h2>         
                </a>
            </div>

        <?php
        endwhile;
        ?>
        </div>
        <?php
    endif; // End Loop

    wp_reset_query();



?>
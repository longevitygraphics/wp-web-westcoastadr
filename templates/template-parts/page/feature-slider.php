<?php

$form_active = get_field( 'form_active' );
$h1_title    = get_field( 'h1_title' );

if ( have_rows( 'feature_slider' ) ) :
	?>
		<div class="feature-slider-wrap">
			<div class="feature-slider">
				<?php
				while ( have_rows( 'feature_slider' ) ) :
					the_row();
					$image = get_sub_field( 'image' );
					?>
						<div><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"></div>
						<?php
					endwhile;
				?>
			</div>

			<?php if ( $form_active || $h1_title ) : ?>
				<div class="overlay_wrap">
					<div class="overlay container 
					<?php
					if ( $h1_title ) {
						echo 'justify-content-between';}
					?>
					">
						<?php if ( $h1_title ) : ?>
						<div style="">
							<span class="text-white"> </span>
						</div>
						<?php endif; ?>
						<?php if ( $form_active ) : ?>
							<?php echo do_shortcode( '[gravityform id=1 title=false description=false ajax=true tabindex=49]' ); ?>
						<?php endif; ?>
						<div class="d-flex d-md-none justify-content-center align-items-center flex-column mobile-overlay-text">	
								<div class="h1 text-white">Request a Consultation</div>
								<a href="tel:<?php echo format_phone( do_shortcode( '[lg-phone-main]' ) ); ?>" class="btn btn-primary"><?php echo format_phone( do_shortcode( '[lg-phone-main]' ) ); ?></a>
						</div>
					</div>
				</div>
			<?php endif; ?>

		</div>
	<?php
else :
	// no rows found
endif;

?>

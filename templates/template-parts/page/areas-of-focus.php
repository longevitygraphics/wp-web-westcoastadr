<div class="py-5 container">
	<h2 class="mb-4 text-center h1">Areas of Focus</h2>

	<?php
		$args = array(
	        'showposts'	=> -1,
	        'post_type'		=> 'service'
	    );

	    $result = new WP_Query( $args );

	    // Loop
	    if ( $result->have_posts() ) :
	    	?>
	    	<div class="service-list">
	    	<?php
	        while( $result->have_posts() ) : $result->the_post(); 
	        	$image = get_the_post_thumbnail_url();
	        	$title = get_the_title();
	        	$url = get_permalink();
	    ?>
	    	
	      		<div>
	      			<img src="<?php echo $image; ?>">

	      			<p><a href="<?php echo $url; ?>"><?php echo $title; ?></a></p>
	      		</div>

			<?php
	        endwhile;
	        ?>
	        </div>
	        <?php
	    endif; // End Loop

	    wp_reset_query();
	?>
</div>
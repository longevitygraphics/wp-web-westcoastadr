<div class="mobile-utilty">
    <div>
      <nav class="">  
          <?php 
            $mobileUtility = array(
              'menu'              => 'mobile-utility-menu',
              'depth'             => 1,
              'container'         => 'div'
            );
            wp_nav_menu($mobileUtility);
          ?>
      </nav>
    </div>
</div>
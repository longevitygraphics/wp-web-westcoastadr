<?php 
/**
 * General Text block component.
 *
 */
?>

<?php 
	
	// Background Colours
	$background_color = get_sub_field('background_colour'); 
	$background_image = $background_color['background_image'];
	
	if ( $background_color['background_image'] && $background_color['background_colour'] == 'bg-image') {
		$background_image = ' style="background-image:url(' . $background_color['background_image'] . ')" '; 
	} 

	// Padding & Margin
	$block_padding = get_sub_field('block_padding');
	$block_margin  = get_sub_field('block_margin'); 
	$item_padding  = $block_padding['padding'] . '-' . $block_padding['size'];
	$item_margin   = $block_margin['margin'] . '-' . $block_margin['size'];


	// Block Fields
	$video = get_sub_field('youtube_video');
	$image = get_sub_field('image');
	$block_title = get_sub_field('block_title');
	$media_type = get_sub_field('media_type');
	$image_slider = get_sub_field('image_slider');
	$maximum_width = get_sub_field('maximum_width');
?> 


<section class="flexible-item grid-layout <?php echo $class; ?> <?php the_sub_field('custom-classes'); ?> <?php echo $background_color['background_colour']; ?> <?php echo $item_margin; ?>" <?php echo $background_image; ?> >
	<div class="<?php the_sub_field('container'); ?>  <?php echo $item_padding; ?>">
		<?php if(get_sub_field('block_title_show') == 1): ?>
			<h2 class="text-center mb-4"><?php echo $block_title; ?></h2>
		<?php endif; ?>
		<div class="row media <?php the_sub_field('align_items_vertical'); ?> <?php the_sub_field('align_items_horizontal'); ?>" style="max-width:<?php echo $maximum_width; ?>; margin-left:auto; margin-right:auto;">
			<?php if($media_type == 'Video'): ?>
				<div class="embed-responsive embed-responsive-16by9">
					<?php echo $video; ?>
				</div>
			<?php endif; ?>

			<?php if($media_type == 'Image'): ?>
				<div class="media-image">
					<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
				</div>
			<?php endif; ?>

			<?php if($media_type == 'Image Slider'): ?>
				<?php if($image_slider && is_array($image_slider)): ?>
					<div class="flexible-content-slider">
					<?php foreach ($image_slider as $key => $slides): ?>
						<div>
							<img src="<?php echo $slides['slides']['link']; ?>" alt="<?php echo $slides['slides']['alt']; ?>">
						</div>
					<?php endforeach; ?>
					</div>

					<script>
						(function($) {

						    $(document).ready(function(){
						    	$('.flexible-content-slider').slick({
						    		slidesToShow: 1,
									slidesToScroll: 1,
									arrows: false,
									dots: true,
									fade: true,
						    		variableWidth: false,
						    		adaptiveHeight: true
								});
						    });

						}(jQuery));
					</script>
				<?php endif; ?>
			<?php endif; ?>
				
		</div>
	</div>
</section>
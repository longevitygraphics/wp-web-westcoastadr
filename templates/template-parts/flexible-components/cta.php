	<?php 
				
		// Background Colours
		$background_color = get_sub_field('background_colour'); 
		$background_image = $background_color['background_image'];
		
		if ( $background_color['background_image'] && $background_color['background_colour'] == 'bg-image') {
			$background_image = ' style="background-image:url(' . $background_color['background_image'] . ')" '; 
		} 

		// Padding & Margin
		$block_padding = get_sub_field('block_padding');
		$block_margin  = get_sub_field('block_margin'); 
		$item_padding  = $block_padding['padding'] . '-' . $block_padding['size'];
		$item_margin   = $block_margin['margin'] . '-' . $block_margin['size'];

	?> 



	<div class="<?php echo $item_padding; ?> cta-block <?php the_sub_field('custom-classes'); ?> <?php echo $background_color['background_colour']; ?> <?php echo $item_margin; ?>" <?php echo $background_image; ?> >
		<div class="<?php the_sub_field('container'); ?>  ">
			<div class="cta-title" >
				<?php if($cta_title): ?>
					<p class="text-white h3 mb-1"><?php echo $cta_title; ?></p>
				<?php endif; ?>
				<?php if($cta_subtitle): ?>
					<div class="text-white"><?php echo $cta_subtitle; ?></div>
				<?php endif; ?>
			</div>
		
		<?php if($cta_button): ?>
			<a href="<?php echo $cta_button['url']; ?>" class="btn btn-secondary"><?php echo $cta_button['title']; ?></a>
		<?php endif; ?>
		</div>
	</div>
</div>
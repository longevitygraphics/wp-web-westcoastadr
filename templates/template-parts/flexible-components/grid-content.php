<?php 
/**
 * General Text block component.
 *
 */
?>

<?php 
	
	// Background Colours
	$background_color = get_sub_field('background_colour'); 
	$background_image = $background_color['background_image'];
	
	if ( $background_color['background_image'] && $background_color['background_colour'] == 'bg-image') {
		$background_image = ' style="background-image:url(' . $background_color['background_image'] . ')" '; 
	} 

	// Padding & Margin
	$block_padding = get_sub_field('block_padding');
	$block_margin  = get_sub_field('block_margin'); 
	$item_padding  = $block_padding['padding'] . '-' . $block_padding['size'];
	$item_margin   = $block_margin['margin'] . '-' . $block_margin['size'];


	// Block Fields
	$block_title = get_sub_field('block_title');
	$left_content_type = get_sub_field('left_content_type');
	$right_content_type = get_sub_field('right_content_type');

	$left_content_copy = get_sub_field('left_content_copy');
	$left_content_image = get_sub_field('left_content_image');
	$right_content_copy = get_sub_field('right_content_copy');
	$right_content_image = get_sub_field('right_content_image');

	$left_width = get_sub_field('left_width');
	$right_width = get_sub_field('right_width');

	if($left_content_type == 'Text' && $right_content_type == 'Text'){
		$combination = 1;
		$class = 'extra-padding';
	}elseif($left_content_type == 'Text' && $right_content_type == 'Image'){
		$combination = 2;
		$class = '';
	}elseif($left_content_type == 'Image' && $right_content_type == 'Text'){
		$combination = 3;
		$class = '';
	}
?> 


<section class="flexible-item grid-layout <?php echo $class; ?> <?php the_sub_field('custom-classes'); ?> <?php echo $background_color['background_colour']; ?> <?php echo $item_margin; ?>" <?php echo $background_image; ?> >
	<div class="<?php the_sub_field('container'); ?>  <?php echo $item_padding; ?>">
		<?php if(get_sub_field('block_title_show') == 1): ?>
			<h2 class="text-center mb-4"><?php echo $block_title; ?></h2>
		<?php endif; ?>
		<div class="row <?php the_sub_field('align_items_vertical'); ?> <?php the_sub_field('align_items_horizontal'); ?>">
			<?php if($left_content_type == 'Text'): ?>
				<div class="col left col-12 col-md-<?php echo $left_width; ?> d-flex flex-column order-2 px-4"><?php echo $left_content_copy; ?></div>
			<?php else: ?>
				<div class="col left col-12 col-md-<?php echo $left_width; ?> d-flex flex-column order-1"><div class="image"><img src="<?php echo $left_content_image['url']; ?>" alt="<?php echo $left_content_image['alt']; ?>"></div></div>
			<?php endif; ?>

			<?php if($right_content_type == 'Text'): ?>
				<div class="col right col-12 col-md-<?php echo $right_width; ?> d-flex flex-column  order-2 px-4"><?php echo $right_content_copy; ?></div>
			<?php else: ?>
				<div class="col right col-12 col-md-<?php echo $right_width; ?> d-flex flex-column  order-1 px-4"><div class="image"><img src="<?php echo $right_content_image['url']; ?>" alt="<?php echo $left_content_image['alt']; ?>"></div></div>
			<?php endif; ?>
		</div>
	</div>
</section>
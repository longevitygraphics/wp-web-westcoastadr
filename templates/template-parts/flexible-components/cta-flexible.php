<?php if( have_rows('flexible_content_block') ):
	while ( have_rows('flexible_content_block') ) : the_row();
		switch ( get_row_layout()) {

			// Text Block
			case 'text_block':
				get_template_part('/templates/template-parts/flexible-components/text-block');
			break;
			
			// Copy Image Block
			case 'grid_content':
				get_template_part('/templates/template-parts/flexible-components/grid-content');
			break;

			// Copy Image Block
			case 'grid_-_list':
				get_template_part('/templates/template-parts/flexible-components/grid-list');
			break;

			// Buttons Block
			case 'buttons':
				get_template_part('/templates/template-parts/flexible-components/buttons');
			break;

			// Video Block
			case 'full_width_media':
				get_template_part('/templates/template-parts/flexible-components/media');
			break;

			// Video Block with Overlay
			case 'full_width_media_with_overlay':
				get_template_part('/templates/template-parts/flexible-components/media-with-overlay');
			break;

			// Block Title
			case 'section_title':
				get_template_part('/templates/template-parts/flexible-components/block-title');
			break;

			// Bloc CTA
			case 'block_cta':
				$cta_title = get_sub_field('cta_title');
				$cta_subtitle = get_sub_field('cta_subtitle');
				$cta_button = get_sub_field('cta_button');

				include(locate_template('/templates/template-parts/flexible-components/cta.php'));
			break;

			default:
				echo "<!-- nothing to see here -->";
			break;
		}

	endwhile; else : // no layouts found 
endif; ?>
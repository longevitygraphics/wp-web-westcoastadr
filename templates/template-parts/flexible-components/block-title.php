<?php 
/**
 * General Text block component.
 *
 */
?>

<?php 
	
	// Background Colours
	$background_color = get_sub_field('background_colour'); 
	$background_image = $background_color['background_image'];
	
	if ( $background_color['background_image'] && $background_color['background_colour'] == 'bg-image') {
		$background_image = ' style="background-image:url(' . $background_color['background_image'] . ')" '; 
	} 

	// Padding & Margin
	$block_padding = get_sub_field('block_padding');
	$block_margin  = get_sub_field('block_margin'); 
	$item_padding  = $block_padding['padding'] . '-' . $block_padding['size'];
	$item_margin   = $block_margin['margin'] . '-' . $block_margin['size'];


	// Block Fields
	$title = get_sub_field('title');
?> 


<section class="flexible-item grid-layout <?php echo $class; ?> <?php the_sub_field('custom-classes'); ?> <?php echo $background_color['background_colour']; ?> <?php echo $item_margin; ?>" <?php echo $background_image; ?> >
	<div class="<?php the_sub_field('container'); ?>  <?php echo $item_padding; ?>">
		<h2 class="text-center mb-0"><?php echo $title; ?></h2>
	</div>
</section>
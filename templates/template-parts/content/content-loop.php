<?php
$args = array(
	'orderby' => 'date',
	'order' => 'DESC',
);
// The Query
$the_query = new WP_Query( $args );
// The Loop
 if ( $the_query->have_posts() ): ?>
    <?php while ( $the_query->have_posts() ): ?>
      <?php  $the_query->the_post(); ?>
		
		<article>
			<header>
				<h2><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h2>
				<p class="small">by&nbsp;<?php the_author_link(); ?> on <?php the_date( '', '', '', true ); ?></p>
			</header>
			<div class="article-content">
				<p><?php the_excerpt(); ?></p>
			</div>
			<footer class="small">
				Categories: <?php echo get_the_category_list(', '); ?> <br>
				<?php echo get_the_tag_list('Tags:&nbsp;', ', '); ?>
				<hr>
			</footer>
		</article>

	<?php endwhile; ?>
<?php endif?> 





<div class="contact-method row">
	<div class="col-sm-6 mb-3">
		<div class="title">Call Us Today</div>
        <a href="tel:<?php echo do_shortcode('[lg-phone-main]'); ?>"><?php echo do_shortcode('[lg-phone-main]'); ?></a><br>
        <a href="tel:<?php echo do_shortcode('[lg-phone-alt]'); ?>"><?php echo do_shortcode('[lg-phone-alt]'); ?></a>
	</div>
	<div class="col-sm-6 mb-3">
		<div class="title"><a href="mailto:<?php echo do_shortcode('[lg-email]'); ?>">Email Us</a></div>
        <div class="title">Connect with Us</div>
        <?php echo do_shortcode('[lg-social-media]'); ?>
	</div>
</div>

<div class="contact-address mb-3">
	<div class="title">Our Location</div>
	<?php echo do_shortcode('[lg-address1]'); ?><br>
	<?php echo do_shortcode('[lg-address2]'); ?><br>
	<?php echo do_shortcode('[lg-city]'); ?>, <?php echo do_shortcode('[lg-province]'); ?> <?php echo do_shortcode('[lg-postcode]'); ?>
</div>
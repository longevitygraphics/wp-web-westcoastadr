<div class="footer-logo d-md-flex">
    <?php echo has_custom_logo() ? get_custom_logo() : do_shortcode('[lg-site-logo]'); ?>
    <p class="ml-3 copyright-text">
    	<span class="d-block">Copyright &copy; <?php echo date("Y") ?> 
    		<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" class="color-secondary"><?php bloginfo( 'name' ); ?></a> All Rights Reserved
    	</span>
    </p>
</div>
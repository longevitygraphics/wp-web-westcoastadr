<div class="title">All Services</div>

<?php
	$args = array(
        'showposts'	=> -1,
        'post_type'		=> 'service'
    );

    $result = new WP_Query( $args );

    // Loop
    if ( $result->have_posts() ) :
    	?>
    	<div class="footer-services-list">
    	<?php
        while( $result->have_posts() ) : $result->the_post(); 
        	$title = get_the_title();
        	$url = get_permalink();
    	?>
    	
       		<div>
       			<a href="<?php echo $url; ?>"><?php echo $title; ?></a>
       		</div>

		<?php
        endwhile;
        ?>
        </div>
        <?php
    endif; // End Loop

    wp_reset_query();
?>

<div class="container pt-2 d-none d-lg-block">
    <?php get_template_part("/templates/template-parts/footer/logo"); ?>
</div>  
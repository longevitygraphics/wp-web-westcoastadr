<?php
/**
 * Main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 */

get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content">

			<?php get_template_part( '/templates/template-parts/page/feature-slider' ); ?>

			<main>

				<?php get_template_part( '/templates/template-parts/flexible-components/cta-flexible' ); ?>

				<?php get_template_part( '/templates/template-parts/page/buttons' ); ?>

				<?php get_template_part( '/templates/template-parts/page/areas-of-focus' ); ?>

				<div class="service-cta bg-primary py-4">
					<div class="container">
					<?php 
						$cta_title = get_field('cta_title');
						$cta_subtitle = get_field('cta_subtitle');
						$cta_button = get_field('cta_button');
						include(locate_template('/templates/template-parts/flexible-components/cta.php', false, false));
					?>
					</div>
				</div>
			</main>

		</div>
	</div>
<?php get_footer(); ?>
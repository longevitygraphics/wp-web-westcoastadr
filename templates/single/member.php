<?php

get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content">

			<main class="single-member">
				<?php
					$title                              = get_the_title();
					$url                                = get_permalink();
					$member_image                       = get_field( 'member_image' );
					$member_certs                       = get_field( 'member_certs' );
					$member_title                       = get_field( 'member_title' );
					$member_description                 = get_field( 'member_description' );
					$education_bar_admission_and_awards = get_field( 'education_bar_admission_and_awards' );
				?>
				<div class="container py-5">
					<div>
						<h1 class="h3" style="display:inline;"><?php echo $title; ?></h1> 
						<h2 class="h3" style="display:inline;"> 
						<?php
						if ( $member_certs ) {
							echo ', ' . $member_certs; }
						?>
							<?php if ( $member_title ) : ?>
								<br><span style="font-size: 70%;" class="h4"><?php echo $member_title; ?></span>
							<?php endif; ?>
						</h2>
					</div>
					<img class="mr-4 mb-4" style="float:left;" src="<?php echo $member_image['url']; ?>" alt="<?php echo $member_image['alt']; ?>">
					<?php echo $member_description; ?>

					<?php if ( $education_bar_admission_and_awards ) : ?>
						<div class="mt-3">
							<?php echo $education_bar_admission_and_awards; ?>
						</div>
					<?php endif; ?>
				</div>
			
			</main>
		</div>
	</div>

<?php get_footer(); ?>

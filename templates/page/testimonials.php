<?php
/**
 * Main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 */

get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content">

			<?php get_template_part( '/templates/template-parts/page/feature-slider' ); ?>

			<main>

				
				

				<?php get_template_part( '/templates/template-parts/flexible-components/cta-flexible' ); ?>


				<?php
					$args = array(
				        'showposts'	=> -1,
				        'post_type'		=> 'testimonials'
				    );

				    $result = new WP_Query( $args );

				    // Loop
				    if ( $result->have_posts() ) :
				    	?>
				    	<div class="testimonials-list py-5 container">
				    	<?php
				        while( $result->have_posts() ) : $result->the_post(); 
				        	$title = get_the_title();
				        	$url = get_permalink();
				    	?>
				    	
			                <div class="mason-grid-item">
			                    <h3><?php echo $title ?></h3>
			                    <p><?php the_content(); ?></p> 
			                </div>   

						<?php
				        endwhile;
				        ?>
				        </div>
				        <?php
				    endif; // End Loop

				    wp_reset_query();
				?>
				

			</main>
		</div>
	</div>
<?php get_footer(); ?>
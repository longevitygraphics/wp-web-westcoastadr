<?php

get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content">

			<?php get_template_part( '/templates/template-parts/page/feature-slider' ); ?>

			<main>

				<?php get_template_part( '/templates/template-parts/flexible-components/cta-flexible' ); ?>

				<!-- Contact Form -->
				<div class="container pb-3 pb-md-5">
					<?php echo do_shortcode('[gravityform id=2 title=false description=false ajax=true tabindex=49]'); ?>
				</div>
				<!-- end Contact Form -->

				<!-- Contact Method -->
				<div class="contact-contact container pb-4">
					<div class="row">
						<div class="col-md-6">
							<table>
								<tr>
									<td><i class="fas fa-map-marker-alt"></i></td>
									<td><?php echo do_shortcode('[lg-address2]'); ?><br><?php echo do_shortcode('[lg-city]'); ?>, <?php echo do_shortcode('[lg-province]'); ?> <?php echo do_shortcode('[lg-postcode]'); ?></td>
								</tr>
								<tr>
									<td><a href="mailto:<?php echo do_shortcode('[lg-email]'); ?>"><i class="far fa-envelope"></i></a></td>
									<td><a href="mailto:<?php echo do_shortcode('[lg-email]'); ?>"><?php echo do_shortcode('[lg-email]'); ?></a></td>
								</tr>
								<tr>
									<td><a href="tel:<?php echo do_shortcode('[lg-phone-main]'); ?>"><i class="fas fa-phone"></i></a></td>
									<td><a href="tel:<?php echo do_shortcode('[lg-phone-main]'); ?>"><?php echo do_shortcode('[lg-phone-main]'); ?></a></td>
								</tr>
							</table>
						</div>
						<div class="col-md-6">
							<?php echo do_shortcode('[lg-map id=119]'); ?>
						</div>
					</div>
				</div>
				<!-- end Contact Method -->

			</main>
		</div>
	</div>

<?php get_footer(); ?>
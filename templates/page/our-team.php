<?php

get_header(); ?>

    <div id="primary">
        <div id="content" role="main" class="site-content">

            <?php get_template_part( '/templates/template-parts/page/feature-slider' ); ?>

            <main>

                <?php get_template_part( '/templates/template-parts/flexible-components/cta-flexible' ); ?>


                <?php get_template_part( '/templates/template-parts/page/team' ); ?>

            </main>
        </div>
    </div>

<?php get_footer(); ?>
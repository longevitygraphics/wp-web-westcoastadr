<?php

get_header(); ?>

    <div id="primary">
        <div id="content" role="main" class="site-content">

            <?php get_template_part( '/templates/template-parts/page/feature-slider' ); ?>

            <main>
                <h1 class="mt-4" style="text-align: center"><?php echo the_title(); ?></h1>

                <div style="max-width: 602px; margin: auto auto;" class="py-5">
                    <div class="flow-chart">
                        <div class="invlovment-container">
                            <div class="involvment">
                                <span class="more-involve">More Involvement/Control of Process</span>
                                <span class="less-involve">Less Involvement/Control of Process</span>
                            </div>
                        </div>
                        <div class="flow-categories">
                            <img src="/wp-content/themes/wp-web-westcoastadr/assets/dist/images/flow-catogories-top-image.jpg" class="flow-catogories-top-image" alt="Top Arrow Image">
                            <div class="catogery-mediation">
                                <h2 class="mt-0-force category-title">Meditation</h2>
                                <div class="flow-child-element">
                                    <h2 class="mt-0-force">Pre-Mediation Meeting 
                                                            <span>(days/weeks)</span>
                                                        </h2>
                                </div>
                                <div class="flow-child-element flow-child-element-last">
                                    <h2 class="mt-0-force">Mediation (one or more) 
                                                            <span>(days/weeks)</span>
                                                        </h2>
                                </div>
                                <div class="flow-child-element-duration">
                                    <h2 class="mt-0-force">Immediate</h2>
                                </div>
                                <div class="flow-child-element-final">
                                    <h2 class="mt-0-force">Final Agreement</h2>
                                </div>
                            </div>
                            <div class="catogery-arbitration">
                                <h2 class="mt-0-force category-title">Arbitration</h2>
                                <div class="flow-child-element">
                                    <h2 class="mt-0-force">Pre-Arbitration Meeting 
                                                            <span>(days/weeks)</span>
                                                        </h2>
                                </div>
                                <div class="flow-child-element">
                                    <h2 class="mt-0-force">Pre-Hearing Meeting 
                                                            <span>(days/weeks)</span>
                                                        </h2>
                                </div>
                                <div class="flow-child-element flow-child-element-last">
                                    <h2 class="mt-0-force">Arbitration 
                                                            <span>(days/weeks)</span>
                                                        </h2>
                                </div>
                                <div class="flow-child-element-duration">
                                    <h2 class="mt-0-force">60 – 90 days</h2>
                                </div>
                                <div class="flow-child-element-final">
                                    <h2 class="mt-0-force">Arbitration Award</h2>
                                </div>
                            </div>
                            <div class="catogery-court">
                                <h2 class="mt-0-force category-title">Court</h2>
                                <div class="flow-child-element">
                                    <h2 class="mt-0-force">Commence Court Process 
                                                            <span>(days/weeks)</span>
                                                        </h2>
                                </div>
                                <div class="flow-child-element judisial-case">
                                    <h2 class="mt-0-force">Judicial Case Conference with a Judge 
                                                            <span>(2-4 months)</span>
                                                        </h2>
                                </div>
                                <div class="flow-child-element flow-child-element-last interim-hearings">
                                    <h2 class="mt-0-force">Interim Hearings, Examination for Discovery 
                                                            <span>(weeks/months)</span>
                                                        </h2>
                                </div>
                                <div class="flow-child-element flow-child-element-last">
                                    <h2 class="mt-0-force">Trial 
                                                            <span>(6 – 12 months)</span>
                                                        </h2>
                                </div>
                                <div class="flow-child-element-duration">
                                    <h2 class="mt-0-force">3 – 12 months</h2>
                                </div>
                                <div class="flow-child-element-final">
                                    <h2 class="mt-0-force">Court Order</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>

            </main>
        </div>
    </div>

<?php get_footer(); ?>
<?php
/**
 * Main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 */

get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content">

			<?php get_template_part( '/templates/template-parts/page/feature-slider' ); ?>

			<main>

				<?php get_template_part( '/templates/template-parts/flexible-components/cta-flexible' ); ?>

				<!-- Articles -->
					<div class="py-5 container">
						<?php
						if( have_rows('articles') ):
							?>
							<h1 class="h2"><strong>Articles</strong></h2>
							<ul>
							<?php
						    while ( have_rows('articles') ) : the_row();
						        $title = get_sub_field('title');
						        $author = get_sub_field('author');
						        $link_type = get_sub_field('link_type');
						        $link = get_sub_field('link');
						        $file = get_sub_field('file');
						        ?>
						        	<li>
						        	<?php if($link_type == 'Link'): ?>
										<?php if ($link &&  is_array($link) && array_key_exists("url", $link ) ): ?> 
											<a href="<?php echo $link['url']; ?>"><?php echo $title; ?></a>
										<?php else: ?>
											<?php echo $title; ?>
										<?php endif; ?>
										
							        <?php elseif($link_type == 'File'): ?>
										<?php if ($file &&  is_array($file) && array_key_exists("url", $file ) ): ?>
											<a href="<?php echo $file['url']; ?>"><?php echo $title; ?></a>
										<?php else: ?>
											<?php echo $title; ?>
										<?php endif ?>
							    	<?php endif; ?>
							    		<br><span><?php echo $author; ?></span>
							    	</li>
						        <?php
						    endwhile;
						    ?>
						    </ul>
						    <?php
						else :
						    // no rows found
						endif;
						?>
					</div>
				<!-- end Articles -->

			</main>
		</div>
	</div>
<?php get_footer(); ?>
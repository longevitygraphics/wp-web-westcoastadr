<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>
	
	<div class="scroll-top">
		<i class="fas fa-chevron-up"></i>
	</div>

	<footer id="colophon" class="site-footer">
		
		<div id="site-footer" class="clearfix py-4 px-3">
			<div class="container">
				<div class="footer-services">
					<?php get_template_part("/templates/template-parts/footer/services"); ?>
				</div>
				<div class="footer-contact">
					<?php get_template_part("/templates/template-parts/footer/contacts"); ?>
				</div>
			</div>

			<div class="container d-lg-none">
				<?php get_template_part("/templates/template-parts/footer/logo"); ?>
			</div>		
		</div>

	</footer><!-- #colophon -->

<?php wp_footer(); ?>

</body>
</html>

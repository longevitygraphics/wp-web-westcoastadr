<?php

	/* Include theme functions */
	$function_path = get_stylesheet_directory() . '/functions';

	$file_path = [
		'/basic/theme-supports.php',
		'/basic/enqueue_styles_scripts.php',
		'/helper.php',
		'/basic/custom-post-types.php',
		'/basic/custom-taxonomy.php',
		'/basic/custom-menus.php',
		'/basic/widgets.php',
		'/advanced/wp-bootstrap-navwalker.php',
		'/advanced/actions-filters.php',
		'/advanced/client-roles.php',
		'/include/template-override.php',
		'/tinymce.php'
	];

	foreach ($file_path as $key => $value) {
		if (file_exists($function_path . $value)) {
		    require_once($function_path . $value);
		}
	}
	/* end */

?>
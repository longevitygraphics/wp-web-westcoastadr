<?php
/**
 * Template for displaying 404 pages (Not Found)
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

get_header(); ?>

	<div id="primary" class="full-page">
		<div id="content" role="main">

			<div class="py-5 text-center">
				<div class="container">
					<div class="h1 font-weight-bold mb-4">404</div>
					<div class="h4">Looks like the page you're looking for does not exist.</div>
					<div class="mt-5"><a class="btn btn-primary" href="<?php echo get_site_url(); ?>">BACK TO HOME</a></div>
				</div>
			</div>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>
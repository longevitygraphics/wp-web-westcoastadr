// Windows Ready Handler

(function($) {



    $(document).ready(function(){

      	//Mobile Toggle
        $('.mobile-toggle').on('click', function(){
      		$('.navbar-collapse').fadeToggle();
      	});

        $('.dropdown-toggle').on('click', function(e){
          e.preventDefault();
        });

        $('.dropdown-toggle').closest('li').on('mouseover', function(e){
          $(this).addClass('open');
        });

        $('.dropdown-toggle').closest('li').on('mouseleave', function(e){
          $(this).removeClass('open');
        });

        $('.feature-slider').slick({
          autoplay: true,
          fade: true,
          arrows: true,
          dots: false,
          autoplaySpeed: 6000,
          adaptiveHeight: true
        });

        $('.testimonials-list').masonry({
          // options
          itemSelector: '.mason-grid-item',
          });   

        $('.scroll-top').on('click', function(e) {
          e.preventDefault();
          var scrollTo = $('#primary');
          var settings = {
            duration: 2 * 1000,
            offset: ($('.site-header').outerHeight()) * -1
          };
          //console.log()
          KCollection.headerScrollTo(scrollTo, settings);
        });
      //If Mega Menu
      /*$('.mobile-toggle').on('click', function(){
        $('.mega-menu-toggle').click();
      });*/
        
    });

}(jQuery));
function sticky_header_initialize(){
	var headerHeight = jQuery('#masthead').outerHeight();
	jQuery('.site-content').css('paddingTop', headerHeight);
	jQuery('#masthead').fadeIn();

	if(jQuery(window).width() < 769){
		jQuery('nav.navbar').css('max-height', 'calc( 100vh - '+ headerHeight +'px )');
	}else{
		jQuery('nav.navbar').css('max-height', 'auto');
	}
}

<?php

function create_taxonomy(){

	register_taxonomy(
		'testimonial-category', //Taxonmy Slug
		'testimonials', //CPT 
		array(
			'label' => __( 'Category' ),
			'hierarchical' => true,
		)
	);

	register_taxonomy(
		'member-category', //Taxonmy Slug
		'member', //CPT 
		array(
			'label' => __( 'Category' ),
			'hierarchical' => true,
		)
	);
}

add_action( 'init', 'create_taxonomy' );

?>
<?php

require_once get_stylesheet_directory() . '/functions/include/custom-post-type.php';


    // SERVICE
    
    register_post_type( 'service',
        array(
          'labels' => array(
            'name' => __( 'Services' ),
            'singular_name' => __( 'Service' )
          ),
          'public' => true,
          'has_archive' => false,
          "rewrite" => array("with_front" => false, "slug" => 'service'),
          'menu_icon'   => 'dashicons-location',
          'show_in_menu'    => 'westcoastadr',   #### Main menu slug
          'supports' => array( 'thumbnail','title', 'editor', 'excerpt' )
        )
    );

    // MEMBER
    
    register_post_type( 'member',
        array(
          'labels' => array(
            'name' => __( 'Team Members' ),
            'singular_name' => __( 'Team Member' )
          ),
          'public' => true,
          'has_archive' => false,
          "rewrite" => array("with_front" => false, "slug" => 'member'),
          'menu_icon'   => 'dashicons-location',
          'show_in_menu'    => 'westcoastadr',   #### Main menu slug
          'supports' => array( 'thumbnail','title', 'editor', 'excerpt' )
        )
    );

    // TESTIMONIAL
    
    register_post_type( 'testimonials',
        array(
          'labels' => array(
            'name' => __( 'Testimonials' ),
            'singular_name' => __( 'Testimonials' )
          ),
          'public' => true,
          'has_archive' => false,
          'menu_icon'   => 'dashicons-format-quote',
          'show_in_menu'    => 'westcoastadr',   #### Main menu slug
          'supports' => array( 'thumbnail','title', 'editor', 'excerpt' ),
          'taxonomies'  => array( 'testimonial-category' )
        )
    );

?>
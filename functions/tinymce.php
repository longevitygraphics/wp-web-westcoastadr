<?php

	// Register format button
	function my_mce_buttons_2( $buttons ) {
		array_unshift( $buttons, 'styleselect' );
		return $buttons;
	}

	add_filter( 'mce_buttons_2', 'my_mce_buttons_2' );


	// Insert items to format button
	function my_mce_before_init_insert_formats( $init_array ) { 

		$title_size = ['h1', 'h2', 'h3', 'h4', 'h5', 'h6'];
		$title_size_array = [];

		if($title_size && is_array($title_size)){
			foreach ($title_size as $key => $value) {
				array_push($title_size_array, array(
					'title' => ucwords($value),
		            'selector' => 'span,div,h1,h2,h3,h4,h5,h6,p,a',
		            'classes' => $value
				));
			}
		}


	    $style_formats = array(
	    	array(
	            'title' => 'Title Size',
	            'items' =>  $title_size_array
	        )
	    );

	    $init_array['style_formats'] = json_encode( $style_formats );

	    return $init_array; 
	  
	} 

	add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' );  

?>